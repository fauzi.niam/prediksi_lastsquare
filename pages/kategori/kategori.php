<?php
include_once("koneksi.php");
$database = new Connection(); 
$db = $database->openConnection();
$sql="SELECT * FROM `tb_kategori`";
$dat = $db->query($sql);
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Daftar Kategori</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <div align="center" ><a class="btn btn-info" href="?pg=kategori_form"><i class="fa fa-plus fa-fw"></i> Tambah</a></div>
            </div>
            <div class="panel-body">
                <table class="table">
                  <tr>
                    <th>No</th>
                    <th>Kategori</th>
                    <!-- <th>Kategori Menu</th> -->
                    <th>Aksi</th>
                  </tr>
                <?php
                while ($data=$dat->fetch(PDO::FETCH_ASSOC)){ 
                $i++;?>
                  <tr>
                    <td><?=$i?></td>
                    <td><?=$data["kategori"]?></td>
                    <!-- <td><?=$data["menu_kategori"]?></td> -->
                    <td><a class="btn btn-info" href="?pg=kategori_form&act=edit&id_kategori=<?=$data["id_kategori"]?>"><i class="fa fa-pencil fa-fw"></i> Edit</a>&nbsp;
                <a class="btn btn-danger" href="?pg=kategori_hapus&id_kategori=<?=$data["id_kategori"]?>"><i class="fa fa-trash-o fa-fw"></i>Hapus</a></td>
                  </tr>
                <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>