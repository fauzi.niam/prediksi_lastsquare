<?php
include_once("koneksi.php");
include_once 'Perhitungan/Leastsquare.php';
    $least= new \Perhitungan\Leastsquare;
    $database = new \Connection; 
    $db = $database->openConnection();
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <h1>Prediksi</h1>
        </div>
        <div class="pull-right">
            <p id="realtgl" class="datetime"></p>
            <p id="realwaktu" class="datetime"></p>
        </div>
    </div>
</div>
<hr style="margin-top: 0px; ">
<div class="row">
    <div class="col-md-12">   
        <div class="panel panel-default">
            <div class="panel-heading">
                Prediksi Stok Tahun Depan
            </div>
            <div class="panel-body">
                <?php 
                    $sql = "SELECT  *, SUM(`jumlah`) AS jml, YEAR(tgl_transaksi) AS tahun, MONTH(tgl_transaksi) bulan FROM `tb_transaksi` `a`  LEFT JOIN `tb_transaksi_detail` `b`  ON b.id_transaksi = a.`id_transaksi` LEFT JOIN `tb_menu` `c` ON c.id_menu=b.id_menu GROUP BY b.id_menu, YEAR(`tgl_transaksi`) order by b.`id_menu` asc, `tahun` asc";
                    
                    $data_res= $db->query($sql)->fetchAll();
                    // print_r($data_res);

                    $new_array=array();

                    $no=0;
                    $last_idmenu='';
                    foreach ($data_res as $row) {
                        // echo " ID: ".$row['id_menu'] . "<br>";           
                        // echo " Name: ".$row['tahun'] . "<br>";
                        if($last_idmenu==''){
                            $last_idmenu=$row['id_menu'];
                        }

                        if($last_idmenu==$row['id_menu']){
                            $new_array[$no]['id_menu']=$row['nama_menu'];
                            $new_array[$no]['data'][]=array(
                                // 'id_menu'=>$row['id_menu'],
                                'tahun'=>$row['tahun'],
                                'penjualan'=>$row['jml']
                            );
                        }

                        if($last_idmenu!=$row['id_menu']){
                            $last_idmenu=$row['id_menu'];
                            $no=$no+1;

                            $new_array[$no]['id_menu']=$row['nama_menu'];
                            $new_array[$no]['data'][]=array(
                                // 'id_menu'=>$row['id_menu'],
                                'tahun'=>$row['tahun'],
                                'penjualan'=>$row['jml']
                            );
                        }
                    }

                    // print_r($new_array);
                    $prediksi=array();

                    foreach ($new_array as $value) {
                                  
                        $jml_data=count($value['data']);
                        $data_analisis=$least->analisis($value['data'],$jml_data);

                        $jml_penjualan_y=$least->sum_penjualan_y($data_analisis);
                        $jml_prediksi_x=$least->sum_prediksi_x($data_analisis);
                        $jml_x_2=$least->sum_x_2($data_analisis);
                        $jml_x_y=$least->sum_x_y($data_analisis);

                        $nilai_a=$least->nilai_a($jml_penjualan_y, $jml_data);
                        $nilai_b=$least->nilai_b($jml_x_y, $jml_x_2);

                        $sesudah=$least->sesudah($data_analisis, $nilai_a, $nilai_b, 1);
                        
                        $prediksi[]=array(
                            'menu'=>$value['id_menu'],
                            'prediksi'=>$sesudah
                        );
                    }

                    // print_r($prediksi);

                    // print_r($new_array[0]['data']);

                    // $jml_data=count($new_array[0]['data']);
                    // $data_analisis=$least->analisis($new_array[0]['data'],$jml_data);

                    // $jml_penjualan_y=$least->sum_penjualan_y($data_analisis);
                    // $jml_prediksi_x=$least->sum_prediksi_x($data_analisis);
                    // $jml_x_2=$least->sum_x_2($data_analisis);
                    // $jml_x_y=$least->sum_x_y($data_analisis);

                    // $nilai_a=$least->nilai_a($jml_penjualan_y, $jml_data);
                    // $nilai_b=$least->nilai_b($jml_x_y, $jml_x_2);

                    // $sesudah=$least->sesudah($data_analisis, $nilai_a, $nilai_b, 1);// 3 tahun sesudah
                    // $sebelum=$least->sebelum($data_analisis, $nilai_a, $nilai_b, 1);// 2 tahun sebelum

                    // // print_r($data) ;
                    // // echo $nilai_a.'-'.$nilai_b;
                    // echo ' <br> '.$sesudah;
                    // echo ' <br> '.$sebelum;
                ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Menu</th>
                            <th>Prediksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no_dat=1;
                            foreach ($prediksi as $row) {
                                echo '
                                    <tr>
                                        <td>'.$no_dat.'</td>
                                        <td>'.$row['menu'].'</td>
                                        <td>'.$row['prediksi'].'</td>
                                    </tr>
                                ';
                                $no_dat++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>