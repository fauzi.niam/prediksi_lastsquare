<div class="col-md-4 col-md-offset-4 form-login" style="margin-top: 100px;">
    
    <?php
    /* handle error */
    if (isset($_GET['msg'])) : ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <strong>Warning!</strong> <?=$_GET['msg'];?>
        </div>
    <?php endif;?>
 
        <div class="outter-form-login well">
        <!-- <div class="logo-login">
            <em class="glyphicon glyphicon-user"></em>
        </div> -->
            <form action="login.php" class="inner-login" method="post">
            <h3 class="text-center title-login">Login Admin</h3>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
 
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                
                <input type="submit" class="btn btn-block btn-custom-green" value="LOGIN" />
                
            </form>
        </div>
    </div>