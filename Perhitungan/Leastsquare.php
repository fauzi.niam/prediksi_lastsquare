<?php 
namespace Perhitungan;

/**
 * summary
 */
class Leastsquare
{
    /**
     * summary
     */
    function analisis($array=array(), $jml_data)
    {
    	$data_analisis=array();

    	if(($jml_data % 2) == 1){
    		$nilai_tengah=round($jml_data/2)+1;
    		$nilai_awal=-round($jml_data/2);

    		foreach ($array as $val) {
    			$data_analisis[]=array(
    				'tahun'=>$val['tahun'],
                    'bulan'=>$val['bulan'],
    				'penjualan_y'=>$val['penjualan'],
    				'prediksi_x'=>$nilai_awal+1,
    				'x_2'=>pow($nilai_awal+1, 2),
    				'x_y'=>($nilai_awal+1)*$val['penjualan']
    			);
    			$nilai_awal=$nilai_awal+1;
    		}

    	}else{
    		$nilai_tengah=($jml_data/2);
    		$nilai_awal=-1;
    		for ($i = 0; $i < $nilai_tengah; $i++) {		
    			$nilai_awal=$nilai_awal-2;
    		}

    		foreach ($array as $val) {
    			$data_analisis[]=array(
    				'tahun'=>$val['tahun'],
                    'bulan'=>$val['bulan'],
    				'penjualan_y'=>$val['penjualan'],
    				'prediksi_x'=>$nilai_awal+2,
    				'x_2'=>pow($nilai_awal+2, 2),
    				'x_y'=>($nilai_awal+2)*$val['penjualan']
    			);
    			$nilai_awal=$nilai_awal+2;
    		}
    	}
    	return $data_analisis;
    }

    function sum_penjualan_y($analisis=array())
    {
    	return array_sum(array_column($analisis, 'penjualan_y'));
    }

    function sum_prediksi_x($analisis=array())
    {
    	return array_sum(array_column($analisis, 'prediksi_x'));
    }

    function sum_x_2($analisis=array())
    {
    	return array_sum(array_column($analisis, 'x_2'));
    }

    function sum_x_y($analisis=array())
    {
    	return array_sum(array_column($analisis, 'x_y'));
    }

    function nilai_a($penjualan_y, $jml_data)
    {
    	$nilai_a=$penjualan_y/$jml_data;
    	return $nilai_a;
    }

    function nilai_b($x_y, $x_2)
    {
    	$nilai_b=$x_y/$x_2;
    	return $nilai_b;
    }

    function sesudah($data_analisis, $nilai_a, $nilai_b,$tahun)
    {
    	$data=array();
    	$jml_data=count($data_analisis);
    	$x=$data_analisis[$jml_data-1]['prediksi_x'];
    	if ( $jml_data % 2 ) {
			for ($i = 0; $i < $tahun; $i++) {
				$x=$x+1;
	    		$data[]=ceil($nilai_a+($nilai_b*$x));
	    	}	    		
    	} else {
    		for ($i = 0; $i < $tahun; $i++) {
	    		$x=$x+2;
	    		$data[]=ceil($nilai_a+($nilai_b*$x));
	    	}
    	}

    	return implode(',', $data);
    }

    function sebelum($data_analisis, $nilai_a, $nilai_b, $tahun)
    {
    	$data=array();
    	$jml_data=count($data_analisis);
    	$x=$data_analisis[0]['prediksi_x'];
    	if ( $jml_data % 2 ) {
			for ($i = 0; $i < $tahun; $i++) {
				$x=$x-1;
	    		$data[]=ceil($nilai_a+($nilai_b*$x));
	    	}	    		
    	} else {
    		for ($i = 0; $i < $tahun; $i++) {
	    		$x=$x-2;
	    		$data[]=ceil($nilai_a+($nilai_b*$x));
	    	}
    	}

    	return implode(',', $data);
    }
}
